class rocm::install inherits rocm {

  file { "$install_dir/rocm-smi":
    ensure => present,
    source => 'puppet:///modules/rocm/rocm-smi',
    owner  => 'root',
    group  => 'root',
    mode   => '0744'
  }
}